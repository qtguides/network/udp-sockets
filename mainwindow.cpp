#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QtDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    udpServer = nullptr;

    ui->setupUi(this);

    ui->clientePuerto->setText("7755");
    ui->clienteMensaje->setText("Hola!");
    ui->clienteDireccion->setText("127.0.0.1");

    ui->servidorPuerto->setText("7755");

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::receive(QString msg)
{
    ui->servidorMensajes->addItem(msg);
}

void MainWindow::clienteEnviar()
{
    qDebug() << "Cliente Enviar";

    QUdpSocket socket;
    QHostAddress addr(ui->clienteDireccion->text());
    QString mensaje(ui->clienteMensaje->text());
    uint16_t port = static_cast<uint16_t>(QString(ui->clientePuerto->text()).toInt());

    socket.writeDatagram(mensaje.toUtf8(), addr, port);
}

void MainWindow::servidorConectar()
{
    if(udpServer)
        delete udpServer;

    QString port(ui->servidorPuerto->text());
    udpServer = new UdpServer(static_cast<unsigned short>(port.toInt()), this, this);

    qDebug() << "Servidor Conectar";
}

void MainWindow::on_actionAcerca_de_triggered()
{
    qDebug() << "UNRN UDP demo!";
}
