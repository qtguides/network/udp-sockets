#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "udpserver.h"
#include "msgreceiver.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow, public MsgReceiver
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow() override;
    void receive(QString msg) override;

private:
    Ui::MainWindow *ui;
    UdpServer * udpServer;

public slots:
    void clienteEnviar();
    void servidorConectar();
private slots:
    void on_actionAcerca_de_triggered();
};

#endif // MAINWINDOW_H
