#ifndef MSGRECEIVER_H
#define MSGRECEIVER_H

#include <QString>

class MsgReceiver
{
public:
    virtual void receive(QString msg) = 0;
    virtual ~MsgReceiver() { }
};

#endif // MSGRECEIVER_H
