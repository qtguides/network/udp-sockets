#include "udpserver.h"
#include <QNetworkDatagram>

UdpServer::UdpServer(unsigned short port, MsgReceiver *r, QObject *parent)
    : QObject(parent)
{
    receiver = r;
    udpSocket = new QUdpSocket(this);
    udpSocket->bind(QHostAddress::Any, port);

    connect(udpSocket, SIGNAL(readyRead()), this, SLOT(readPendingDatagrams()));
}

void UdpServer::readPendingDatagrams()
{
    while (udpSocket->hasPendingDatagrams()) {
        QNetworkDatagram datagram = udpSocket->receiveDatagram();
        qDebug() << datagram.data();
        receiver->receive(datagram.data());
    }
}
