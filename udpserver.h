#ifndef UDPSENDER_H
#define UDPSENDER_H

#include <QObject>
#include <QUdpSocket>
#include "msgreceiver.h"

class UdpServer : public QObject
{
    Q_OBJECT

private:
    QUdpSocket  * udpSocket;
    MsgReceiver * receiver;

public:
    UdpServer(unsigned short port, MsgReceiver *r, QObject *parent = nullptr);

public slots:
    void readPendingDatagrams();
};

#endif // UDPSENDER_H
